import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../model/user';
import { RegisterService } from '../register.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers : [MessageService]
})
export class HomeComponent implements OnInit {
  userForm : FormGroup;
  loading = false;
  submitted = false;
  display : boolean = false;

  constructor(private formBuilder: FormBuilder,private registerService : RegisterService,private messageService : MessageService) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      fname: ['', Validators.required],
      lname : ['',Validators.required],
      emailId: ['',Validators.required],
     
      password: ['', Validators.required],
    });
  }
  get f() { return this.userForm.controls; }

  showDialog(): void {
    this.userForm.reset();
    this.display = true;
  }

  onCancel(): void {
    this.userForm.reset();
    this.display = false;
  }
  
  saveUser() {
    if (this.userForm.invalid) {
      return;
    }

    const user = this.user;

    this.registerService.registerUser(user).subscribe(res => {
      if(res.status === 200) {
        this.displayMessage('success', 'User Created');
        this.onCancel();
      }
    },error => {
      this.displayMessage('error', 'Creation Failed');
    })
    

  }

  displayMessage(severity: string, message: string) {
    this.messageService.clear();
    this.messageService.add({ severity: severity, summary: message });
  }

  get user(): User {
    const userVal = this.userForm.value;
    let user = new User(userVal.fname,userVal.lname,userVal.emailId,userVal.password);
    return user;
  }

}
