import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthService } from '../../auth/auth.service'
import { Router } from '@angular/router';
import {ConfirmationService} from 'primeng/api';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers : [ConfirmationService]
})
export class HeaderComponent implements OnInit {
  userName : string;
  authSubscription: Subscription;
 
  constructor(private authService : AuthService, private route: Router,private confirmationService : ConfirmationService) {
    this.authSubscription = this.authService.user_type.subscribe(value => {
      console.log('UserType :' + value.user_type);
      this.userName = value.user_name;
      });
   }

  ngOnInit(): void {
  }

  logout() : void {
    this.confirmationService.confirm({
      message: 'Hi'+ " " +this.userName +',Are you sure that you want to logout?',
      accept: () => {
        this.authService.logout();
          //Actual logic to perform a confirmation
      }
  });
   
  }

  login() : void {
    this.route.navigate(['/signin']);
  }
}
