import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from '../app-routing.module';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { FooterComponent } from './footer/footer.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { BrowserModule } from '@angular/platform-browser';
import { DialogModule } from 'primeng/dialog';
import {RadioButtonModule} from 'primeng/radiobutton';
import {ToastModule} from 'primeng/toast';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {ConfirmationService} from 'primeng/api';
import { MessageService } from 'primeng/api';



@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    RadioButtonModule,
    AppRoutingModule,
    CommonModule,
    MenubarModule,
    ToastModule,
    ButtonModule,
    ConfirmDialogModule,
    DialogModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    AppRoutingModule
  ],
  providers: [ConfirmationService,MessageService]
})
export class CoreModule { }