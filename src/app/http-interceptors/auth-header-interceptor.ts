import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';


@Injectable() 
export class AuthHeaderInterceptor implements HttpInterceptor {
    token:string = "";
    intercept(request: HttpRequest<any>,next: HttpHandler) {

        this.token = localStorage.getItem("id_token");
        request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${this.token}`
            }
          });

        return next.handle(request);
    }
    
}