import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { SigninComponent } from './signin/signin.component';
import { AuthRoutingModule } from './auth-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHeaderInterceptor } from '../http-interceptors/auth-header-interceptor';



@NgModule({
  declarations: [SigninComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    AuthRoutingModule
  ],exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  providers : [{
    provide: HTTP_INTERCEPTORS,
      useClass: AuthHeaderInterceptor,
      multi: true
  }]
})
export class AuthModule { }
