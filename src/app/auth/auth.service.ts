import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import * as moment from "moment";
import * as jwt_decode from "jwt-decode";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn: boolean = false;

  public user_type = new BehaviorSubject({
    user_name: localStorage.getItem('user_name'),
    email_id: localStorage.getItem('email_id'),
    user_type: localStorage.getItem('user_type')
  });
  constructor(private router: Router, private http: HttpClient) { }

  signinUser(email: string, password: string) {
    let cred = {
      email: email,
      password: password
    }
    return this.http.post('/api/vi/user/login', cred, { observe: 'response' }).subscribe(res => {
      console.log(JSON.stringify(res.body));
      let cred = JSON.parse(JSON.stringify(res.body));
      let token = cred.token;
      const expiresIn = moment().add(cred.expiresIn, 'second');
      console.log('token' + token + ":" + "expiresIn" + expiresIn);
      let tokenInfo = this.getDecodedAccessToken(token);
      console.log('tokenInfo' + JSON.stringify(tokenInfo));

      localStorage.setItem('id_token', token);
      localStorage.setItem("expires_at", JSON.stringify(expiresIn.valueOf()));
      localStorage.setItem('user_name', tokenInfo.user_name);
      localStorage.setItem('email_id',tokenInfo.emailid);
      localStorage.setItem('user_type', tokenInfo.user_type);
      this.user_type.next({
        user_name: localStorage.getItem('user_name'),
        email_id : localStorage.getItem('email_id'),
        user_type: localStorage.getItem('user_type')
      });
      if(tokenInfo.user_type == 'U') {
          this.router.navigate(['user/normaluser'])
      } else if(tokenInfo.user_type == 'A') {
        this.router.navigate(['/user/admin']);
      }
      
    })
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  isAuthenticated(): boolean {
    return moment().isBefore(this.checkExpiration());
  }

  checkExpiration() {
    const expiration = localStorage.getItem("expires_at");
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  isLoggedOut() {
    return !this.isAuthenticated();
  }

  logout(): void {
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("user_name");
    localStorage.removeItem("email_id");
    localStorage.removeItem("user_type");
    this.router.navigate(['/signin']);
  }


}
