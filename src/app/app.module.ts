import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
//import { AuthModule } from './auth/auth.module';
import {UserModule} from './user/user.module'
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { MessageService } from 'primeng/api';
import { DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
      ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ConfirmDialogModule,
    CoreModule,
    AuthModule,
    UserModule
  ],
  providers: [ConfirmationService,MessageService,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
