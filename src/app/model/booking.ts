export class booking {
    constructor(
        public trainid?: string,
        public pnrnumber?: string,
        public userid?: string,
        public seats?: string,
        public dateBooked?: string
        
    ) { }
}