export class User {
    constructor(
       public fname: string = '',
       public lname : string = '',
       public emailid: string = '',
       public password: string = ''
    ) { }
}