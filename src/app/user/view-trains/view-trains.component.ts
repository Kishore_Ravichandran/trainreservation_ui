import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service'

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/auth/auth.service'
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-view-trains',
  templateUrl: './view-trains.component.html',
  styleUrls: ['./view-trains.component.css'],
  providers: []
})
export class ViewTrainsComponent implements OnInit {
  authSubscription: Subscription;
  user_type: string = '';
  user_name: string = '';
  trainList = [];
  constructor(private adminService: AdminService) { }

  ngOnInit(){
    this.getTrainList();
  }

  getTrainList(){
    this.adminService.getTrainList().subscribe(train => {
      for(var i =0 ;i<train.length ;i++) {
        this.trainList.push({
          trainid: train[i].trainid,
          fromDest : train[i].fromdest,
          toDest : train[i].todest,
          date : new Date(train[i].date)
        })
      }
    })
  }

}
