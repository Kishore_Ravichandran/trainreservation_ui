import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  getBookingList():Observable<any> {
      return this.http.get('/api/vi/admin/booking');
  }

  getTrainList():Observable<any> {
    return this.http.get('/api/vi/admin/trainList');
  }

  updateBooking(booking): Observable<any> {
    return this.http.post("/api/vi/admin/updateBooking", booking);
  }
}