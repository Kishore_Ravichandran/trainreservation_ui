import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  searchTrains(info): Observable<any> {
    return this.http.post("/api/vi/user/searchTrain", info);
  }

  bookTicket(info): Observable<any> {
    return this.http.post("/api/vi/user/bookTicket", info);
  }
}