import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service'
import { ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/auth/auth.service'
import { Subscription } from 'rxjs';
import {booking} from '../../model/booking'

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class BookingComponent implements OnInit {
  bookingForm: FormGroup;
  isView: boolean = false;
  display: boolean = false;
  authSubscription: Subscription;
  user_type: string = '';
  user_name: string = '';
  bookings = [];
    constructor(private confirmationService: ConfirmationService, private fb: FormBuilder, private messageService: MessageService,private authService: AuthService,private adminService: AdminService) { 

    this.authSubscription = this.authService.user_type.subscribe(value => {
      console.log('UserType :' + value.user_type);
      this.user_name = value.user_name;
      this.user_type = value.user_type;
    });

  }

  ngOnInit(){

    this.bookingForm = this.fb.group({
      name: [''],
      pnrno: [''],
      emailid: [''],
      seats:['']
    })
    this.getBookings();
  }

  getBookings() {
    this.adminService.getBookingList().subscribe(booking => {
      if(booking) {
        for(var i = 0 ;i<booking.length;i++) {
          this.bookings.push({
            trainid: booking[i].trainid,
            pnrnumber : booking[i].pnrnumber,
            fromdest : booking[i].fromdest,
            todest : booking[i].todest,
            doj : booking[i].doj,
            seats : booking[i].seats,
            userid:booking[i].userid
          })
        }
        console.log('Booking' + JSON.stringify(booking));
        
      } 
    }), error => {
      console.log('Error to get the booking list', error);
    };
  }
  
  onRowEditSave(booking) {
    this.adminService.updateBooking(booking).subscribe(res => {
      if(res.status === 200) {
        
        for (var i = 0; i < this.bookings.length; i++) {
          if (this.bookings[i].pnrnumber === booking.pnrnumber) {
            this.bookings[i].trainid = booking.trainid;
            this.bookings[i].seats = booking.seats;
            this.bookings[i].userid = booking.userid;
            break;
          }
        }
        this.messageService.add({severity:'success', summary: 'Success', detail:'Booking is updated'});
      } else {
        this.messageService.add({severity:'error', summary: 'Error', detail:'Booking Cannot be updated'});
      }
    })
  }

}


