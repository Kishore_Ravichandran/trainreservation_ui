import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';

import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { AdminComponent } from './admin/admin.component';
import {NormalUserComponent} from './normal-user/normal-user.component'
import { UserRoutingModule } from './user-routing.module';
import { BookingComponent } from './booking/booking.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHeaderInterceptor } from '../http-interceptors/auth-header-interceptor';


import { ViewTrainsComponent } from './view-trains/view-trains.component';


@NgModule({
  declarations: [
    AdminComponent,
    //UserRoutingModule,
    NormalUserComponent,
    BookingComponent,
    
    ViewTrainsComponent
  ],
  imports: [
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    TabViewModule,
    CardModule,
    DialogModule,
    ButtonModule,
    ConfirmDialogModule,
    InputTextModule,
    CalendarModule,
    RadioButtonModule,
    TableModule,
    DropdownModule,
    ToastModule
    
  ],
  providers : [{
    provide: HTTP_INTERCEPTORS,
      useClass: AuthHeaderInterceptor,
      multi: true
  }]
})
export class UserModule { }
