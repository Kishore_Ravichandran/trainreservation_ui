import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin/admin.component';
import {NormalUserComponent} from './normal-user/normal-user.component'
import {AuthGuard} from '../auth/auth.guard'


const userRoutes: Routes = [
  { path: 'user/admin', component: AdminComponent,canActivate:[AuthGuard]},
  { path: 'user/normaluser', component: NormalUserComponent ,canActivate:[AuthGuard]},
  
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
