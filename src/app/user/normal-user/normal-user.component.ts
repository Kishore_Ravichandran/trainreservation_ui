import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/auth/auth.service'
import { Subscription } from 'rxjs';
import { SearchInfo} from '../../model/searchInfo'
import { UserService } from '../services/user.service';




@Component({
  selector: 'app-normal-user',
  templateUrl: './normal-user.component.html',
  styleUrls: ['./normal-user.component.css']
})
export class NormalUserComponent implements OnInit {
  searchForm : FormGroup;
  cities = [];
  coach = [];
  searchResults = [];
  trainId: string = "";
  fromStation:string = "";
  toStation: string = "";
  dateofJourney : string = "";
  displayBooking: boolean = false;
  selectedCoach:string = "";
  bookingDetails = [];

  constructor(private fb: FormBuilder,private userService : UserService,private datePipe : DatePipe) { 
    
  }

  ngOnInit(){
    this.searchForm = this.fb.group({
      fromcity: ['',Validators.required],
      tocity : ['',Validators.required],
      journey_date : ['',Validators.required]
      
    })

    this.coach = [
      {name: '3AC', code: '3AC'},
    {name: '2AC', code: '2AC'},
    {name: '1AC', code: '1AC'},
    {name: 'SL', code: 'SL'},
    {name: 'Unreserved', code: 'URS'}
  ];

  this.cities = [
    {name: 'Tirunelveli', code: 'TNV'},
    {name: 'Madurai', code: 'MDR'},
    {name: 'Chennai', code: 'CHN'},
    {name: 'Trichy', code: 'TRY'},
    {name: 'Erode', code: 'ERD'}
  ]
  }

  submit() {
    if(this.searchForm.invalid) {
      return;
    }

    const searchInfo = this.searchInfo;
    this.userService.searchTrains(searchInfo).subscribe(train => {
      if(train) {
        for(var i = 0 ;i<train.length;i++) {
          this.searchResults.push({
            trainid: train[i].trainid,
            fromdest : train[i].fromdest,
            todest : train[i].todest,
            date:train[i].date
          })
        }
        console.log('Booking' + JSON.stringify(this.searchResults));
      }
    },err => {
      console.log('Error to get the Train list', err);
    })
    
  }

  get searchInfo():SearchInfo {
    const search = this.searchForm.value;
    let journey_date = search.journey_date;
    if(search.journey_date instanceof Date){
      journey_date = this.datePipe.transform(search.journey_date, 'yyyy-dd-MM');
    }
    let searchInfo = new SearchInfo(search.fromcity.name,search.tocity.name,journey_date);
    return searchInfo;
  }

  book(trainResults) {
    this.displayBooking = true;
    this.trainId = trainResults.trainid;
    this.fromStation = trainResults.fromdest;
    this.toStation = trainResults.todest;
    this.dateofJourney = trainResults.date;
  }

  proceedBooking() {
    this.bookingDetails = [];
    this.bookingDetails.push({
      trainid: this.trainId,
      fromdest: this.fromStation,
      todest : this.toStation,
      date: this.dateofJourney,
      seats:this.selectedCoach,
      userid: localStorage.getItem('email_id')
    })

    this.userService.bookTicket(this.bookingDetails[0]).subscribe(result => {
      if(result) {
        this.displayBooking = false;
      }
    })
  }

}
